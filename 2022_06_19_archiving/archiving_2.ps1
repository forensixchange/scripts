# Input bindings are passed in via param block.
param($Timer)

# parameters
$SubscriptionId = $env:subscriptionId
$LogAnalyticsResourceGroup = $env:rgName
$LogAnalyticsWorkspaceName = $env:workspaceName
$TenantID = $env:tenantId
$TotalRetentionInDays = $env:totalRetentionInDays #TotalRetentionInDays - the already configured retention period for the table = archive period 

try{
    $ExcludedTables = $env:excludedTables.Split(',')
    $UniqueTables = $env:uniqueTables -replace ",","`r`n" | ConvertFrom-StringData
}
catch{
    Write-Warning "Incorrect values have been provided for the excludedTables or uniqueTables env variables"
    exit
}

Connect-AzAccount -Identity
Set-AzContext -SubscriptionID $SubscriptionId
 
$AzureAccessToken = (Get-AzAccessToken).Token            
$LaAPIHeaders = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
$LaAPIHeaders.Add("Content-Type", "application/json")
$LaAPIHeaders.Add("Authorization", "Bearer $AzureAccessToken")


$TablesApi = "https://management.azure.com/subscriptions/$SubscriptionId/resourcegroups/$LogAnalyticsResourceGroup/providers/Microsoft.OperationalInsights/workspaces/$LogAnalyticsWorkspaceName/tables" + "?api-version=2021-12-01-preview"
$TablesApiResult = Invoke-RestMethod -Uri $TablesApi -Method "GET" -Headers $LaAPIHeaders -MaximumRetryCount 3 -RetryIntervalSec 12

 
foreach ($ta in $TablesApiResult.value){
    $ArchiveDays = $null
    $isExcluded = $false

    foreach($regex_entry in $ExcludedTables)
    {
        if($ta.name.Trim() -match $regex_entry)
        {
            $isExcluded = $true
            break
        }
    }

    if ($ta.properties.Plan -eq 'Analytics' -And !$isExcluded)
    {
		#Analytics tables which are not whitelisted by the ExcludedTables variable

        foreach ($key in $UniqueTables.Keys) { 
            if ($ta.name.Trim() -match $key)
            {
                $ArchiveDays = [int]($UniqueTables[$key])
            }
        }
        if ($ArchiveDays -eq $null){
            #Table is not defined in the UniqueTables variable via regex
            $ArchiveDays = [int]($TotalRetentionInDays)
        }
        
        if([int]$ta.properties.totalRetentionInDays -ne [int]$ArchiveDays )
        {
            $TablesApi = "https://management.azure.com/subscriptions/$SubscriptionId/resourcegroups/$LogAnalyticsResourceGroup/providers/Microsoft.OperationalInsights/workspaces/$LogAnalyticsWorkspaceName/tables/$($ta.name.Trim())" + "?api-version=2021-12-01-preview"
            $TablesApiBody = "{'properties': {'retentionInDays': null,'totalRetentionInDays':$($ArchiveDays)}}"

            try {        
                $TablesApiResult = Invoke-RestMethod -Uri $TablesApi -Method "PUT" -Headers $LaAPIHeaders -Body $TablesApiBody -MaximumRetryCount 3 -RetryIntervalSec 5
                Write-Information "$($ta.name.Trim()) DONE - TotalRetentionInDays: $($ArchiveDays) days"		
            } 
            catch {                    
                Write-Warning -Message "Update-TablesRetention $($_) for table: $($ta.name.Trim())"
            }
        }
    }
}




