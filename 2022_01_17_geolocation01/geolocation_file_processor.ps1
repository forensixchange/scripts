param($input_file, $output_file, $cache_size=500)

# Import-CSV does not read the entire file into memory when piped
$max = (Import-Csv -Path $input_file -Header 'IP1', 'IP2', 'CC', 'Country', 'Area', 'City' | Select-Object @{name='diff'; expression= {$_.IP2-$_.IP1}}| Measure diff -Maximum).Maximum

$divider = [math]::Ceiling($max / 10000000) * 10000000
Write-Host $divider
$cntr = 0
$list = ""

Import-Csv -Path $input_file -Header 'IP1', 'IP2', 'CC', 'Country', 'Area', 'City' | ForEach-Object{
    $bag1 = [math]::Floor($_.IP1 / $divider)
    $bag2 = [math]::Floor($_.IP2 / $divider)
    if ($bag1 -ne $bag2)
        {
            $Z = [math]::Floor($_.IP2 / $divider) * $divider
            $list += ("`"$($_.IP1)`",`"$($Z-1)`",`"$($_.CC)`",`"$($_.Country)`",`"$($_.Area)`",`"$($_.City)`"")
            $list += ("`"$($Z)`",`"$($_.IP2)`",`"$($_.CC)`",`"$($_.Country)`",`"$($_.Area)`",`"$($_.City)`"")
        }
    else    
        {
        $list += ("`"$($_.IP1)`",`"$($_.IP2)`",`"$($_.CC;)`",`"$($_.Country)`",`"$($_.Area)`",`"$($_.City)`"")
        }
    if ($cntr % $cache_size -eq 0)
        {
        $list | Add-Content -Path $output_file
        $list = @()
        }
    $cntr = $cntr + 1
}

$list | Add-Content -Path $output_file




