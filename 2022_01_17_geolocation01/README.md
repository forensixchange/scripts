# Geolocation Enrichment in Sentinel

The above codes are used in [this blogpost.](https://forensixchange.com/posts/22_01_17_geolocation01/)

## PowerShell code to enhance the database file

**geolocatoin_file_processor.ps1** processes the free geolocation database of [IP2Location](http://www.ip2location.com).
The script finds the biggest range and then modifies the IP2Location database accordingly to make it useful for the KQL query without any additional trickery.

Before optimization the code loaded the whole database (200MB) into memory and processed it there. For some reason this needed 5 GB of memory on my machine and 1 hour to finish the execution. After this I decided to pipe the CSV file to the other commands. This way I could keep the memory usage between 60-100 MB and the execution time around 10 minutes.

## KQL query

The uploaded **geolocation_enrichment.kql** file queries the IP2Location database from an external storage and uses this information to enrich IPs in Sentinel with geo data on-the-fly.
