using namespace System.Net

# Input bindings are passed in via param block.
param($Request, $TriggerMetadata)

# Write to the Azure Functions log stream.
Write-Host "PowerShell HTTP trigger function processed a request."

Function Build-Signature ($customerId, $sharedKey, $date, $contentLength, $method, $contentType, $resource)
    {
    $xHeaders = "x-ms-date:" + $date
    $stringToHash = $method + "`n" + $contentLength + "`n" + $contentType + "`n" + $xHeaders + "`n" + $resource
    
    $bytesToHash = [Text.Encoding]::UTF8.GetBytes($stringToHash)
    $keyBytes = [Convert]::FromBase64String($sharedKey)
    
    $sha256 = New-Object System.Security.Cryptography.HMACSHA256
    $sha256.Key = $keyBytes
    $calculatedHash = $sha256.ComputeHash($bytesToHash)
    $encodedHash = [Convert]::ToBase64String($calculatedHash)
    $authorization = 'SharedKey {0}:{1}' -f $customerId,$encodedHash
    return $authorization
}


Function Post-LogAnalyticsData($customerId, $sharedKey, $body, $logType)
{
    $method = "POST"
    $contentType = "application/json"
    $resource = "/api/logs"
    $rfc1123date = [DateTime]::UtcNow.ToString("r")
    $contentLength = $body.Length
    $signature = Build-Signature `
    -customerId $customerId `
    -sharedKey $sharedKey `
    -date $rfc1123date `
    -contentLength $contentLength `
    -fileName $fileName `
    -method $method `
    -contentType $contentType `
    -resource $resource
    $uri = "https://" + $customerId + ".ods.opinsights.azure.com" + $resource + "?api-version=2016-04-01"
    
    $headers = @{
        "Authorization" = $signature;
        "Log-Type" = $logType;
        "x-ms-date" = $rfc1123date;
        "time-generated-field" = $TimeStampField;
    }
    
    $response = Invoke-WebRequest -Uri $uri -Method $method -ContentType $contentType -Headers $headers -Body $body -UseBasicParsing
    return $response.StatusCode
    
}


$Event = New-Object -TypeName PSobject
$Event | add-member -name Host -value $Request.Headers.host -MemberType NoteProperty
$Event | add-member -name UserAgent -value $Request.Headers.'user-agent' -MemberType NoteProperty
$Event | add-member -name CPU -value $Request.Headers.'ua-cpu' -MemberType NoteProperty
$Event | add-member -name ClientIP -value $Request.Headers.'client-ip' -MemberType NoteProperty
$Event | add-member -name SiteID -value $Request.Headers.'x-site-deployment-id' -MemberType NoteProperty
$Event | add-member -name XFF -value $Request.Headers.'x-forwarded-for' -MemberType NoteProperty
$CodeHash = Get-FileHash -Algorithm SHA256 -InputStream ([System.IO.MemoryStream]::New([System.Text.Encoding]::ASCII.GetBytes($Request.Query.code)))
$Event | add-member -name CodeHash -value $CodeHash.Hash -MemberType NoteProperty


$Event = ConvertTo-Json $Event
Write-Host $Event



###################
$CustomerId = "<WORKSPACE_ID>"
$SharedKey = "<PRIMARY_KEY>"
$DataTable = "HoneyDoc_Detection_CL"

$TimeStampField = get-date
$TimeStampField = $TimeStampField.GetDateTimeFormats(115)

$returnvalue = Post-LogAnalyticsData -customerId $customerId -sharedKey $sharedKey -body ([System.Text.Encoding]::UTF8.GetBytes($Event)) -logType $DataTable

Write-Host $returnvalue


###################


Push-OutputBinding -Name Response -Value ([HttpResponseContext]@{
    StatusCode = [HttpStatusCode]::OK
})
