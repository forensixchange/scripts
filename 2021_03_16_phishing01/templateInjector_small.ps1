Param(
    [Parameter(Mandatory=$True,Position=1)]
    [ValidateNotNull()]
    [string]$InputDoc,

    [Parameter(Mandatory=$True,Position=2)]
    [ValidateNotNull()]
    [string]$TargetDot,

    [Parameter(Mandatory=$False,Position=3)]
    [string]$OutputDoc,
    
    [Parameter(Mandatory=$False,Position=4)]
    [string]$TempFolder
)

if(!$PSBoundParameters.ContainsKey('OutputDoc'))
{
    $OutputDoc = $InputDoc
}

if(!$PSBoundParameters.ContainsKey('TempFolder'))
{
    $TempFolder = ".\temp_directory"
}

#Processing the document
New-Item -ItemType directory -Path $TempFolder
Rename-Item -Path $InputDoc -NewName "temp.zip"
Expand-Archive -Path "temp.zip" -DestinationPath $TempFolder
Remove-Item "temp.zip"

$XmlPath =  $TempFolder+"\word\_rels\settings.xml.rels"
[XML]$XmlContent = Get-Content $XmlPath

#Modifying the content
foreach($relationship in $XmlContent.Relationships.Relationship){
    if ($relationship.Type.EndsWith('attachedTemplate'))
    {
        $relationship.Target = $TargetDot
    }
 }

Set-Content -Path $XmlPath -Value $XmlContent
$XmlContent.Save($XmlPath)

#Recreating document
Compress-Archive -Path $TempFolder"\*" -DestinationPath ".\temp.zip"
Remove-Item -Recurse $TempFolder
Rename-Item -Path "temp.zip" -NewName $OutputDoc
